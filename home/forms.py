from django import forms
from .models import UserInput

class status_input(forms.Form):
    # class Meta:
    #     model = input
    #     fields = ['status_user']
    #     widgets = {
    #         'status_user' : forms.CharField(attrs={'placeholder' : 'Masukkan status anda','style' : 'border:0; width:100%'})
    #     }
    status_user = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : '',
        'type' : 'text',
        'maxlength' : '',
        'required' : True,
        'style' : '',
        'label' : '',
    }))