from django.urls import path, include
from django.contrib import admin
from .views import home

urlpatterns = [
    path('admin/', admin.site.urls),
    path('chall', include('chall.urls')),
    path('', home, name = 'home'),
]