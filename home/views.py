from django.shortcuts import render, redirect
from .forms import *
from .models import UserInput

# Create your views here.
# def home(request):
#     return render(request, 'index.html')

def home(request):
    if request.method == "POST":
        form = status_input(request.POST)
        if (form.is_valid()):
            status = UserInput()
            status.status_user = form.cleaned_data['status_user']
            status.save()
        return redirect('/')
    else:
        form = status_input()
        status = UserInput.objects.all().order_by('-upload_time')
        response = {'status' : status, 'form': form}
        return render(request, 'indexnew.html', response)
