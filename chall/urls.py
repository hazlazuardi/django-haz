from django.urls import path
from django.contrib import admin
from .views import chall

urlpatterns = [
    path('', chall, name = 'chall'),
    path('admin/', admin.site.urls)
]