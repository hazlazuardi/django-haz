from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import chall



# Create your tests here.
class Home_UnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/chall')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/chall')
        self.assertEqual(found.func, chall)

    def test_index_contains_npm(self):
        response = Client().get('/chall')
        response_content = response.content.decode('utf-8')
        self.assertIn("NPM: 1906316793", response_content)

    